1. Remoten asettaminen
cd /peto
git remote add origin <git repon url>
=> voit katsoa onko remote mennyt perille: git remote -v

2. Muutoksen tekeminen
Jos tekee muutoksen:
1. joko git forkilla (changes => "Stage" muutokselle jonka haluat) tai "git add ." käyttöliittymällä
2. joko git forkilla (commit description esim. "Tein bäkkärin alun") tai git commit -m "blavlba."
3. git push / git push origin master
=> Voi tulla merge konflikti jos teette muutoksia samaan tiedostoon
=> VS codella on hyvä työkalu sen ratkaisemiseen

3. Projektin rakenne
Koko projektin rakenne:
/peto
-> /frontend
-> .gitignore (node_modules)
-> /backend
-> .gitignore (node_modules)

1. Tietokannan luominen ja taulut
-> Tarvii XAMPP localhost/phpmyadmin

2. Backendin pystytys
Esim VS Code: avaa täysin uusi tyhjä kansio
-> npm init -y
-> npm install express cors body-parser mysql
-> node index.js (tai nodemon index.js)
-> express app kuuntelemaan porttia (vaikka 2000)
-> koita saada vaikka kysely tehtyä GET localhost:2000/pages/homepage (antaa selaimella vastauksen)
-> yhdistä tietokantaan mysql -kirjaston avulla SELECT * FROM pages where id={req.params.joku}; => antaa kyselyyn oikean tuloksen

3. Frontin pystytys
Esim VS Code: avaa täysin uusi tyhjä kansio
- create-react-app <joku_sovelluksen_nimi>
- npm install axios react-router-dom <moment reactstrap> myöhemmin
- npm start => aukeaa selaimessa localhost:3000

4. Frontin toiminnallisuus

Arkkitehtuuri
/src
-> index.js (router, joka ohjaa routeihin, tutorial dokumentaatiosta)
/src/pages (HomePage.js, SubPage.js jne)
/src/layout (Footer.js, Header.js, Content.js)

frontend: localhost:3000
-> backend:
class MainPage
GET localhost:2000/pages/homepage
-> JSON muodossa:
{ "title": "Etusivu", "content": "Morooo..."...}
-> tallenna luokan stateen arvot
-> näytä käyttöliittymällä

frontend: localhost:3000/sub_page
Oma React luokka: class SubPage
GET localhost:2000/pages/sub_page


-> miten saa routen yhdistämään luokkaan?
npm install react-router-dom

5. HALLINTAPANEELI:
localhost:3000/admin
- Pitää pystyä tallentamaan fronttiin (COOKIE/Localstorage), että onko kirjautunut
-> pääsee näkemään hallintapaneeli

-> Luo EditPage -class
-> title, content, url -kentät
-> tallenna:
backend PUT localhost:2000/pages/homepage || body {title: "asddsadsa", "content": "asddsadsa"... }

6. ULKOASU
free css templates (googleen)
https://www.free-css.com/free-css-templates
-> saa suht näppärästi yhdistettyä appiin myöhemmin
