const cors = require('cors');
const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser')
const FormData = require('form-data');
const axios = require('axios');
const moment = require('moment');


const db = mysql.createConnection({
    host     : process.env.HOST || 'localhost',
    user     : process.env.USER || 'root',
    password : process.env.PASSWORD || '',
    database : process.env.DATABASE || 'petodb'
});

//yhdistäminen
db.connect((err) => {
    if(err){
        throw err;
    }console.log('yhdistetty');
});

const app = express();
app.use(cors());
// parse application/json
app.use(bodyParser.json())

//testi tietokantaan, pelaajien data tulee esille JSON muodossa, voidaan hyödyntää frontissa
//HAE KAIKKI PELAAJAT TIETOKANNASTA
app.get('/players', (req, res) => {
    let sql = 'SELECT * FROM players ORDER BY number ASC';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

//HAE YHTEYSHENKILÖT
app.get('/contacts', (req, res) => {
    let sql = 'SELECT * FROM contacts';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

//HAE YHTEISTYÖKUMPPANIT
app.get('/sponsors', (req, res) => {
    let sql = 'SELECT * FROM sponsors';
    let query = db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

//HAE UUTISET
app.get('/news', (req, res) => {
    let sql = 'SELECT * FROM news ORDER BY id DESC';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

//HAE SISÄLTÖÄ SIVUILLE TIETOKANNASTA ID:N PERUSTEELLA, esim id 1 etusivu, id 2 ajankohtaista
app.get('/pages/:id', (req, res) => {
    let sql = `SELECT * FROM pages WHERE id = ${req.params.id} LIMIT 1`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        if (result.length === 1){
            res.json(result[0]);
        } else {
            res.status(404).send("");
        }
    });
});

//app.post /login, select all from admin where user & password lenght === 1
//else return 403

app.post('/login', (req, res) => {
    const user = req.body.user;
    const password = req.body.password;
    let sql = `SELECT * FROM admin WHERE user='${user}' AND password='${password}'`;
    db.query(sql, (err, result) => {
        if(err) {
            console.log(err);
        };
        console.log(result);
        if (result.length === 1){
            res.status(200).send("");
        } else {
            res.status(403).send("");
        }
    });
});

/*
PALAUTTAA SEURAAVAT TIEDOT JSON MUODOSSA:

OTTELUN PÄIVÄMÄÄRÄ
OTTELUN KELLONAIKA
KOTIJOUKKUE
VIERASJOUKKUE
KOTIJOUKKUE KUVAURL
VIERASJOUKKUE KUVAURL
VIERASJOUKKUEEN NIMI
KOTIJOUKKUEEN NIMI
KOTIJOUKKUEEN TEHDYT MAALIT
VIERASJOUKKUEEN TEHDYT MAALIT*/

    function formatGameData(payload) {
        let resultGames = [];
        for (let item of payload) {
            const games = item.Games;
            for (let game of games) {
                resultGames.push({
                    gameDate: game.GameDateDB,
                    gameTime: game.GameTime,
                    homeTeam: game.HomeTeam,
                    awayTeam: game.AwayTeam,
                    homeImg: game.HomeImg,
                    awayImg: game.AwayImg,
                    awayTeamAbbrv: game.AwayTeamAbbrv,
                    homeTeamAbbrv: game.HomeTeamAbbrv,
                    HomeGoals: game.HomeGoals,
                    AwayGoals: game.AwayGoals
                })
            }
        }
        return resultGames;
    }


    
    // const vastustaja = aikaisinPeli.Vieras === 'Juju Joensuu' ? aikaisinPeli.Koti : aikaisinPeli.Vieras;
    // message.channel.send("Seuraava peli " + aikaisinPeli.Pvm + ' ' + aikaisinPeli.Paikka + ', Juju VS ' + vastustaja )

app.get('/games', (req, res) => {
   
    const formdata = new FormData();
    formdata.append("dwl", "0");
    formdata.append("season", "2022");
    formdata.append("stgid", "8627");
    formdata.append("teamid", "1305393612");
    formdata.append("districtid", "0");
    formdata.append("gamedays", "100");
    formdata.append("dog", "2021-05-17");
    
    axios.create({
        headers: formdata.getHeaders()
    }).post("http://www.tilastopalvelu.fi/ih/helpers/getGames.php", formdata)
        .then(apiResult => {
            res.json(formatGameData(apiResult.data));
        })
        .catch(e => console.log(e));
})

app.get('/next_game', (req, res) => {
   
    const formdata = new FormData();
    formdata.append("dwl", "0");
    formdata.append("season", "2021");
    formdata.append("stgid", "8627");
    formdata.append("teamid", "1305393612");
    formdata.append("districtid", "0");
    formdata.append("gamedays", "100");
    formdata.append("dog", "2020-10-24");
    
    axios.create({
        headers: formdata.getHeaders()
    }).post("http://www.tilastopalvelu.fi/ih/helpers/getGames.php", formdata)
        .then(apiResult => {
            res.json(nextGame(formatGameData(apiResult.data)));
        })
        .catch(e => console.log(e));
})

function nextGame(games) {
    let aikaisinPaiva = moment('2050-01-01');
    let aikaisinPeli = null;
    for (let peli of games){
      const paiva = moment(peli.gameDate);
      if (paiva.isBefore(aikaisinPaiva) && (paiva.isSameOrAfter(moment(), 'day'))){
        aikaisinPeli = peli;
        aikaisinPaiva = paiva;
        console.log(aikaisinPeli);
      } 
    }
    return aikaisinPeli;
}

//******** Hallintapaneelin metodit ********/ 

//Pelaajien haku
app.get('/players', (req, res) => {
    let sql = 'SELECT * FROM players';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

//pelaajan lisäys
app.post('/lisaa', (req, res) => {
    let post = {
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        number : req.body.number,
        position: req.body.position,
        linkUrl: req.body.linkUrl
     }
    console.log(req.body)
    let sql = 'INSERT INTO players SET ?';
    db.query(sql, post, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    });
});

//näytä
app.get('/showPlayers', (req, res) => {
    let sql = 'SELECT * FROM players';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});

// poisto
app.delete('/poista/:id', (req, res) => {
    let sql = `DELETE FROM players WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Käyttäjä poistettu');
    });
});

// pelaajien muokkaus
app.put('/paivita/:id', (req, res) => {
    let sql = `UPDATE players SET firstName = '${req.body.firstName}', lastName = '${req.body.lastName}', number = '${req.body.number}', position = '${req.body.position}', linkUrl = '${req.body.linkUrl}'  WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Käyttäjä päivitetty');
    });
});

app.get('/showSponsors', (req, res) => {
    let sql = 'SELECT * FROM sponsors';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});


//Sponsorin lisäys
app.post('/lisaaSponsor', (req, res) => {
    let postSponsor = {
        imgUrl : req.body.imgUrl,
        url : req.body.url
     }
    console.log(req.body)
    let sql = 'INSERT INTO sponsors SET ?';
    db.query(sql, postSponsor, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    });
});

// poisto
app.delete('/poistasponsori/:id', (req, res) => {
    let sql = `DELETE FROM sponsors WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('sponsori poistettu');
    });
});

// sponsorin muokkaus
app.put('/paivitaSponsor/:id', (req, res) => {
    let sql = `UPDATE sponsors SET imgUrl = '${req.body.imgUrl}', url = '${req.body.url}' WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Käyttäjä päivitetty');
    });
});

//Uutisen lisäys
app.post('/lisaaUutinen', (req, res) => {
    let uusiUutinen = {
        title : req.body.newsTitle,
        content : req.body.newsContent
     }
    console.log(req.body)
    let sql = 'INSERT INTO news SET ?';
    db.query(sql, uusiUutinen, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.json(result);
    });
});

// poisto
app.delete('/poistauutinen/:id', (req, res) => {
    let sql = `DELETE FROM news WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('uutinen poistettu');
    });
});

//näytä uutiset
app.get('/showNews', (req, res) => {
    let sql = 'SELECT * FROM news';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.json(results);
    });
});


//******** Hallintapaneelin metodit ********/ 

const PORT = 2000;
app.listen (PORT, () => {
    console.log('Serveri käynnissä portilla', PORT);
});
