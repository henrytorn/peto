-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17.11.2020 klo 19:22
-- Palvelimen versio: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petodb`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`) VALUES
(1, 'admin', 'password');

-- --------------------------------------------------------

--
-- Rakenne taululle `contacts`
--

CREATE TABLE `contacts` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `role`) VALUES
(2, 'Sami Puruskainen', '0445650094', 'sami.puruskainen@gmail.com', 'Päävalmentaja'),
(3, 'Juuso Akkanen', '0406390904', 'juuso.akkanen@eezy.fi', 'Joukkeenjohtaja');

-- --------------------------------------------------------

--
-- Rakenne taululle `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `created_at`) VALUES
(5, '<p>Ohjeistus huonontuneeseen koronavirustilanteeseen</p>', '<p>Suomen koronavirusepidemian tilanne on viime päivinä huonontunut. Jääkiekkoliiton tavoitteena on edelleen se, että jääkiekkoharrastus voisi jatkua ja sarjatoiminta pitää käynnissä. Jotta näin voi tapahtua, edellyttää Jääkiekkoliitto seuroilta yleisten hygieniaohjeiden lisäksi seuraavia toimenpiteitä tästä päivästä alkaen:</p>\n<p><br></p>\n<ul>\n  <li>seuran on määrättävä omiin ottelutapahtumiinsa ja harjoituksiin tuleville yli 15-vuotiaille pelaajille, toimihenkilöille ja katsojille maskipakko. Maskia ei tarvitse käyttää pukukopissa, pelaajapenkillä eikä jäällä. THL:n ohjeituksen mukaan maskia ei tule käyttää, jos se vaikeuttaa kohtuuttomasti hengitystä tai on muita terveyteen liittyviä syitä, jotka estävät maskin käytön. Tietyissä tehtävissä (esim. kahvilassa, toimitsijana) maskin voi korvata myös visiirillä.</li>\n  <li>hallin katsojakapasiteetistä saa olla käytössä enintään 50 % ja turvaväleistä on huolehdittava poikkeuksetta</li>\n  <li>mikäli turvavälejä tai maskipakkoa ei noudateta, on seuran tehtävä päätös ottelutapahtumien ja harjoitusten järjestämiseksi ilman katsojia</li>\n</ul>\n<p>Pyydämme myös edelleen muistuttamaan harrastajia siitä, että pienetkin oireet tarkoittavat, ettei hallille tai muutoin ihmisten ilmoille voi tulla. Tämä voi tarkoittaa lisääntyviä poissaoloja harjoituksista tai peleistä, mutta on äärimmäisen tärkeää viruksen leviämisen ennaltaehkäisyssä.</p>', '2020-11-17 16:06:19');

-- --------------------------------------------------------

--
-- Rakenne taululle `pages`
--

CREATE TABLE `pages` (
  `id` int(255) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`) VALUES
(1, 'PeTo pelaa kaudella 2020-2021 III-Divisioonan lohkoa 8, jossa mukana ovat seuraavat joukkueet <br></br>\r\nPeto (Joensuu)<br/>\r\nWarkis/PiPS (Pieksämäki)<br/>\r\nPoU (Polvijärvi)<br/>\r\nRU (Rantasalmi)<br/>\r\nIPK Team (Iisalmi)<br/>\r\nHurtat (Lieksa)<br/>\r\nKajastus (Kontiolahti)<br/>\r\nSepot (Nurmes) <br/>\r\nOoKoo (Outokumpu)\r\n', 'Joensuun PeTo on joensuulainen jääkiekkojoukkue, joka pelaa jääkiekkoliiton alaisessa sarjassa III-divisioonaa. PeTo perustettiin, jotta Joensuussa saadaan mahdollistettua pelipaikka myös niille, joille junioripolun jälkeen ei löydy pelipaikkaa Joensuusta tai muilta lähialueilta. Pelaajarunko koostuu vuonna 2020-2021 vanhoista Karjalan Koplan pelaajista. Karjalan Kopla ja Jokipojat yhdistyivät vuonna 2020 ja tästä syystä II-divisioonaa pelannut Karjalan Kopla lopetti toimintansa. Joensuun Peto (Alunperin Peli-Toverit) oli joensuulainen jääkiekkoseura, joka yhdistyi 1970 Joensuun Kiekko-Poikien kanssa. Peto jatkoi JoKP:n reservijoukkueena.'),
(2, 'Ohjeistus huonontuneeseen koronavirustilanteeseen', '<b>Suomen</b> koronavirusepidemian tilanne on viime päivinä huonontunut. Jääkiekkoliiton tavoitteena on edelleen se, että jääkiekkoharrastus voisi jatkua ja sarjatoiminta pitää käynnissä. Jotta näin voi tapahtua, edellyttää Jääkiekkoliitto seuroilta yleisten hygieniaohjeiden lisäksi seuraavia toimenpiteitä tästä päivästä alkaen:<br/><br/>\r\n\r\nSeuran on määrättävä omiin ottelutapahtumiinsa ja harjoituksiin tuleville yli 15-vuotiaille pelaajille, toimihenkilöille ja katsojille maskipakko. Maskia ei tarvitse käyttää pukukopissa, pelaajapenkillä eikä jäällä. THL:n ohjeituksen mukaan maskia ei tule käyttää, jos se vaikeuttaa kohtuuttomasti hengitystä tai on muita terveyteen liittyviä syitä, jotka estävät maskin käytön. Tietyissä tehtävissä (esim. kahvilassa, toimitsijana) maskin voi korvata myös visiirillä.\r\nhallin katsojakapasiteetistä saa olla käytössä enintään 50 % ja turvaväleistä on huolehdittava poikkeuksetta mikäli turvavälejä tai maskipakkoa ei noudateta, on seuran tehtävä päätös ottelutapahtumien ja harjoitusten järjestämiseksi ilman katsojia\r\nPyydämme myös edelleen muistuttamaan harrastajia siitä, että pienetkin oireet tarkoittavat, ettei hallille tai muutoin ihmisten ilmoille voi tulla. Tämä voi tarkoittaa lisääntyviä poissaoloja harjoituksista tai peleistä, mutta on äärimmäisen tärkeää viruksen leviämisen ennaltaehkäisyssä.'),
(3, 'tÃ¤mÃ¤ on yhteystiedot title', 'tÃ¤mÃ¤ on yhteystiedot content'),
(4, 'title otteluohjelma', 'content otteluohjelma');

-- --------------------------------------------------------

--
-- Rakenne taululle `players`
--

CREATE TABLE `players` (
  `id` int(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `number` int(3) NOT NULL,
  `linkUrl` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `players`
--

INSERT INTO `players` (`id`, `firstName`, `lastName`, `number`, `linkUrl`, `position`) VALUES
(2, 'Riku', 'Kummunmäki', 1, 'https://www.eliteprospects.com/player/240013/riku-kummunmaki', 'Maalivahti'),
(3, 'Joona', 'Lahtinen', 32, 'https://www.eliteprospects.com/player/229902/joona-lahtinen', 'Maalivahti'),
(4, 'Juuso', 'Akkanen', 4, 'https://www.eliteprospects.com/player/9537/juuso-akkanen', 'Puolustaja'),
(5, 'Antti', 'Miettinen', 6, 'eliteprospects.com/player/104016/antti-miettinen', 'Puolustaja'),
(6, 'Samuli', 'Naukkarinen', 17, 'https://www.eliteprospects.com/player/314465/samuli-naukkarinen', 'Puolustaja'),
(7, 'Aleksi', 'Nykänen', 70, 'https://www.eliteprospects.com/player/222340/aleksi-nykanen', 'Puolustaja'),
(8, 'Jani', 'Oinonen', 5, 'https://www.eliteprospects.com/player/11892/jani-oinonen', 'Puolustaja'),
(9, 'Henry', 'Torn', 7, 'https://www.eliteprospects.com/player/112877/henry-torn', 'Puolustaja'),
(10, 'Joonas', 'Väyrynen', 8, 'https://www.eliteprospects.com/player/66301/joonas-vayrynen', 'Puolustaja'),
(11, 'Sami', 'Eklund', 95, 'https://www.eliteprospects.com/player/112886/sami-eklund', 'Hyökkääjä'),
(12, 'Nemo', 'Huhta', 10, 'https://www.eliteprospects.com/player/100527/nemo-huhta', 'Hyökkääjä'),
(13, 'Teemu', 'Lahtinen', 24, 'https://www.eliteprospects.com/player/152025/teemu-lahtinen', 'Hyökkääjä'),
(14, 'Verneri', 'Paijula', 9, 'https://www.eliteprospects.com/player/199161/verneri-paijula', 'Hyökkääjä'),
(15, 'Joni', 'Pakkanen', 23, 'https://www.eliteprospects.com/player/152191/joni-pakkanen', 'Hyökkääjä'),
(16, 'Markus', 'Penttinen', 50, 'https://www.eliteprospects.com/player/54515/markus-penttinen', 'Hyökkääjä'),
(17, 'Samu', 'Pitkänen', 44, 'https://www.eliteprospects.com/player/22596/samu-pitkanen', 'Hyökkääjä'),
(18, 'Sami', 'Puruskainen', 66, 'https://www.eliteprospects.com/player/7495/sami-puruskainen', 'Hyökkääjä'),
(19, 'Tommi', 'Pöllänen', 13, 'https://www.eliteprospects.com/player/365027/tommi-pollanen', 'Hyökkääjä'),
(20, 'Samu', 'Räsänen', 19, 'https://www.eliteprospects.com/player/78479/samu-rasanen', 'Hyökkääjä'),
(21, 'Janne', 'Sutinen', 71, 'https://www.eliteprospects.com/player/217421/janne-sutinen', 'Hyökkääjä'),
(27, 'Marko', 'Nousiainen', 69, '', 'Maalivahti'),
(28, 'Aku', 'Ankka', 98, '', 'Maalivahti'),
(29, 'Kissa', 'Kakkonen', 99, '', 'Maalivahti'),
(30, 'Iines', 'Ankka', 96, '', 'Maalivahti'),
(31, 'Koira', 'Kolmonne', 98, 'https://www.google.com/search?q=allow+only+numbers+in+textbox&oq=allow+only+numbers+in+&aqs=chrome.3.69i57j0l2j0i22i30l5.10080j0j4&sourceid=chrome&ie=UTF-8', 'Puolustaja'),
(32, 'fjiowjfwa', 'jfiejfejf', 0, 'https://www.twitch.tv/vorhala', 'Maalivahti');

-- --------------------------------------------------------

--
-- Rakenne taululle `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(255) NOT NULL,
  `imgUrl` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `sponsors`
--

INSERT INTO `sponsors` (`id`, `imgUrl`, `url`) VALUES
(1, 'https://www.jetsetbar.fi/web/wp-content/themes/iggo/images/logo.jpg', 'https://www.jetsetbar.fi'),
(2, 'https://scontent-hel3-1.xx.fbcdn.net/v/t1.0-9/19429971_1387773538004333_2786562897680529802_n.jpg?_nc_cat=107&ccb=2&_nc_sid=85a577&_nc_ohc=1H4s0bWfRUsAX-UstdU&_nc_ht=scontent-hel3-1.xx&oh=ebf498f761ffb99f2bed321a0fc02325&oe=5FC472E1', 'https://tukkutiimi.fi'),
(3, 'http://www.blentek.fi/wp-content/uploads/2019/07/blentek_logo_pieni_valk.png', 'https://blentek.fi'),
(4, 'https://scontent-hel3-1.xx.fbcdn.net/v/t1.0-9/72289751_101643254588575_1272440514490138624_n.png?_nc_cat=102&ccb=2&_nc_sid=85a577&_nc_ohc=xVkArn4I6x8AX_66Bb4&_nc_ht=scontent-hel3-1.xx&oh=104d1a2d839c711d0e449c5d3946fe8a&oe=5FC83C80', 'https://www.joensuunlaakeri.fi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
