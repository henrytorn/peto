import React from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import axios from 'axios';
import { BACKEND_URL } from '../constants';
import { Card, Accordion, Button} from 'react-bootstrap';
import moment from 'moment';




export default class AjanKohtaista extends React.Component {
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan b"ackendiltä ja asetetaan luokan tilaan
        axios.get(`${BACKEND_URL}/news`)
        .then(res => this.setState({news: res.data}))
        .catch(e => console.log(e))
    }
    state = {
        news: []
    }
    
    render() {
        const {news} = this.state;
        return (
                <>
                
        <Header />
            <div className ="container">
                    <Accordion defaultActiveKey="0">

                            {news.map((item, i) => (
                                <div key={item.id}>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey={`${i}`}>
                        {moment(item.created_at).format("DD.MM.YYYY")} <div dangerouslySetInnerHTML={{__html: item.title}} />
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey={`${i}`}>
                            <Card.Body>
                            <div dangerouslySetInnerHTML={{__html: item.content}} />
                            </Card.Body>
                    </Accordion.Collapse>
                                </div>
                            ))}

            </Accordion>
            </div>
        <Footer/>
                
        </>
        )
        
                
        }
    }
    