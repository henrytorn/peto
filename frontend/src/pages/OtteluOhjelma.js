import React from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer'
import axios from 'axios';
import { BACKEND_URL, GAMES_URL } from '../constants';
import moment from "moment";

export default class Otteluohjelma extends React.Component {
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan backendiltä ja asetetaan luokan tilaan
        axios.get(GAMES_URL)
        .then(res => this.setState({games: res.data}))
        .catch(e => console.log(e))
    }
    state = {
        games: []
    }
    

    renderGame(game) {
        //const url = "http://www.tilastopalvelu.fi//ih/images/associations/weblogos/200x200/"
        
        return (
                <tbody key={game.gameDate}>
                <tr>
                    <td className="ajat show-mobile">{moment(game.gameDate).format("DD.MM")}</td>
                    <td className="ajat show-desktop">{moment(game.gameDate).format("DD.MM.YYYY")}</td>                
                    <td className="ajat">{game.gameTime}</td>
               
                    <div className="koti">
                    <td> <img className="kuva" alt="logo" src={require(`../images/${game.homeTeam}.png`)}></img> <div className="piilotaNimi">{game.homeTeamAbbrv} </div> </td>
                    </div>
                    <td className="tulossolu">{game.HomeGoals} - {game.AwayGoals}</td>
                    <div className="vieras">
                    <td>  <img className="kuva" alt="logo" src={require(`../images/${game.awayTeam}.png`)}></img> <div className="piilotaNimi">{game.awayTeamAbbrv} </div> </td>
                    </div>
                    </tr>
                </tbody>
        )
    }

    render() {
        return (
            <>
            <Header />
                <div className="container">
                    <b>Otteluohjelma:</b>
                        
                        <table className="table table-dark">
                        <thead id="piilota">
                        <tr>
                            <th scope="col">Päivämäärä</th>
                            <th scope="col">Kellonaika</th>
                            <th scope="col">Kotijoukkue</th>
                            <th scope="col">Tulos</th>
                            <th scope="col">Vierasjoukkue</th>
                        </tr>
                    </thead>
                    {this.state.games.map(game => this.renderGame(game))}
                        </table>
                    {/* Luokan tilan pelaajat käydään läpi ja näytetään */}
                    
                </div>
        <Footer/>
        </>
        )
    }
}

//Päivämäärä: {game.gameDate} klo: {game.gameTime} <br />
//{game.homeImg} {game.homeTeamAbbrv} {game.HomeGoals} VS {game.AwayGoals} {game.awayTeamAbbrv} <br />
//<br />
