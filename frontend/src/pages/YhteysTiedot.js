import React from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import axios from 'axios';
import { BACKEND_URL } from '../constants';

export default class YhteysTiedot extends React.Component {
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan backendiltä ja asetetaan luokan tilaan
        axios.get(`${BACKEND_URL}/contacts`)
        .then(res => this.setState({contacts: res.data}))
        .catch(e => console.log(e))
    }
    state = {
        contacts: []
    }
    
    
    renderContact(contact) {

        return (
            <div className="contacts" key={contact.id}>
                <div className="row contacts">
                    <div className="col contacts">
                   <p className="teksti"> {contact.name}</p> 
                   {contact.role} 
                   <br></br>
                    <i className="fa fa-phone-square"></i>{" " + contact.phone} <br></br>
                    <i className="fa fa-envelope"></i>
                    <a href={`mailto:${contact.email}`}>{" " + contact.email}</a> <br></br><br></br>
                    
                    <div className="viiva-ohut"></div>
                    
                    </div>
                    
                </div>
            </div>
        )
    }

    render() {
        return (
            <>
            <Header />
            
            <div className="container">
                <div className="otsikkoteksti">
                Yhteystiedot
                </div>
                <div className="viiva"></div>
                    {this.state.contacts.map(contact => this.renderContact(contact))}
                    <p className="yht teksti">Muut yhteydenotot osoitteeseen <a href="mailto:joensuunpeto@gmail.com">joensuunpeto@gmail.com</a></p>
                </div>
            <Footer/>
            </>
        )
    }
}