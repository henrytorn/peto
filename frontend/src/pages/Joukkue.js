import React from 'react'
import Header from '../layout/Header'
import Footer from '../layout/Footer'
import axios from 'axios';
import { BACKEND_URL } from '../constants';
import '../logo/PeTo-logo.png';


export default class Joukkue extends React.Component {
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan backendiltä ja asetetaan luokan tilaan
        axios.get(`${BACKEND_URL}/players`)
        .then(res => this.setState({players: res.data}))
        .catch(e => console.log(e))
    }
    state = {
        players: []
    }

    renderMaalivahti(player) {
        if(player.position === "Maalivahti"){
            return (
                <div className="moket" key={player.id}>
                        <div className="col">
                        {"#" +player.number}  <a className="pelaajaLinkki" href={player.linkUrl} target="_blank" rel="noopener noreferrer">{player.firstName} 
                       {" "+ player.lastName}</a> 
                        </div>
                </div>
            )
        }
    }

    renderPuolustaja(player) {
        if(player.position === "Puolustaja"){
            return (
                <div className="pakit" key={player.id}>
                        <div className="col">
                        {"#" +player.number}  <a className="pelaajaLinkki" href={player.linkUrl} target="_blank" rel="noopener noreferrer">{player.firstName} {" "+ player.lastName} </a> 
                        {/* {" "+ player.position} {" "+ player.birthDate} */}
                        </div>
                </div>
            )
        }
    }

    renderHyokkaaja(player) {
        if(player.position === "Hyökkääjä"){
            return (
                <div className="hyokkaaja" key={player.id}>
                        <div className="col">
                        {"#" +player.number}  <a className="pelaajaLinkki" href={player.linkUrl} target="_blank" rel="noopener noreferrer">{player.firstName} 
                       {" "+ player.lastName}</a> 
                        {/* {" "+ player.position} {" "+ player.birthDate} */}
                        </div>
                </div>
            )
        }
    }

    render() {
        return (
            <>
            <Header />
                <div className ="container">
                <div className="otsikkoteksti">
                Pelaajat
                </div>
                <div className="viiva"></div>
                    <div className="maalivahdit">
                        <p className="teksti">Maalivahdit</p>
                        <div className="viiva-ohut"></div>
                            <div className="pelaajat-rivi">
                                {this.state.players.map(player => this.renderMaalivahti(player))}
                            </div>
                    </div>
                    <div className="puolustajat">
                        <p className="teksti">Puolustajat</p>
                        <div className="viiva-ohut"></div>
                            <div className="pelaajat-rivi">
                                {this.state.players.map(player => this.renderPuolustaja(player))}
                            </div>
                    </div>
                    <div className="hyokkaajat">
                        <p className="teksti">Hyökkääjät</p>
                        <div className="viiva-ohut"></div>
                        <div className="pelaajat-rivi">
                            {this.state.players.map(player => this.renderHyokkaaja(player))}
                        </div>
                    </div>
                </div>
            <Footer/>
             </>
        )
    }
}
