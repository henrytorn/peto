import React from 'react';
import Header from '../layout/Header';
import Footer from '../layout/Footer';
import axios from 'axios';
import { BACKEND_URL, GAMES_URL } from '../constants';
import moment from 'moment';

export default class KotiSivu extends React.Component {
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan b"ackendiltä ja asetetaan luokan tilaan
        axios.get(`${BACKEND_URL}/pages/1`)
        .then(res => this.setState({pageContent: res.data}))
        .catch(e => console.log(e))

        axios.get(GAMES_URL)
        .then(res => {
            this.setState({
                games: res.data,
                nextGame: this.getNextGame(res.data)
            })
        })
        .catch(e => console.log(e))
    }
    
    
    state = {
        pageContent: {},
        games: [],
        nextGame: {}
    }

    getNextGame(games) {
        let aikaisinPaiva = moment('2050-01-01');
        let aikaisinPeli = null;
        for (let peli of games){
          const paiva = moment(peli.gameDate);
          if (paiva.isBefore(aikaisinPaiva) && (paiva.isSameOrAfter(moment(), 'day'))){
            aikaisinPeli = peli;
            aikaisinPaiva = paiva;
            console.log(aikaisinPeli);
          } 
        }
        return aikaisinPeli;
    }
    
    renderGame(game) {
        if (!game || !game.homeTeamAbbrv) return null;
        return (
            <div id="nextMatch" >
            <div className="row row-no-padding">
            <div className="col ottelu-teksti1"> <p className="teksti">Seuraava ottelu</p>
                {moment(game.gameDate).format("DD.MM.YYYY")}<br></br>
                    {game.gameTime}
                    </div>
            </div>
                    <div className="row row-no-padding">
                        <div className="col-2 ylimaaraiset"></div>
                        <div className="col ottelu-kuva">
                            <img className="kuva-banner" alt="logo" src={require(`../images/${game.homeTeam}.png`)}></img>
                            <div className="nimi-banner"> <p className="teksti">{game.homeTeamAbbrv}</p></div>
                        </div>
                        <div className="col-1 teksti-banner"><p className="teksti">VS.</p></div>
                        <div className="col ottelu-kuva">
                            <img className="kuva-banner" alt="logo" src={require(`../images/${game.awayTeam}.png`)}></img>
                            <div className="nimi-banner"><p className="teksti">{game.awayTeamAbbrv}</p></div>
                        </div>
                        <div className="col-2 ylimaaraiset"></div>
                    </div>
            </div>

            )
        }

        render() {
           
               return ( 
                <>                 
                 <Header />
                {this.renderGame(this.state.nextGame)}

                <div className="container">
                
                <div className="row"></div>
                        <div className="row">
                            <div className="col-sm"> <img className="picture-left"src="https://hovikuvaaja.com/poupeto/2pp.jpg" alt="peto-kuva1"/> </div>
                            <div className="col-sm kotisivuContent content-upper">{this.state.pageContent.content}</div>
                        </div>
                        <div className="row">
                          
                            <div className="col-sm kotisivuContent content-bottom" dangerouslySetInnerHTML={{__html: this.state.pageContent.title}} ></div>
                            <div className="col-sm"> <img className="picture-right"src="https://hovikuvaaja.com/poupeto/33pp.jpg" alt="peto-kuva2"/> </div>
                        </div> 
                </div>
            <Footer/>
                </>
                )
        }
    }
    