import Axios from 'axios';
import React from 'react'
import { BACKEND_URL } from '../constants';
import Footer from '../layout/Footer';
import Header from '../layout/Header';
import ControlPanel from '../layout/ControlPanel';
import { Button } from 'react-bootstrap';


export default class Admin extends React.Component {

    componentDidMount() {
        const username = localStorage.getItem('username');
        const password = localStorage.getItem('password');
        if (username && password) {
            this.handleClickLogin(username, password);
        }
    }

    state = {
        user: "",
        password: "",
        isLogged: false,
        players: []
    }
   
    changeText = e => this.setState({ [e.target.name]: e.target.value });

    isLogged() {
        return this.state.isLogged;
    }

    handleClickLogin = (user, password) => {
        Axios.post(`${BACKEND_URL}/login`, {user, password})
        .then(res => {
            const success = res.data.trim() === "OK";
            if (success) {
                localStorage.setItem('username', user);
                localStorage.setItem('password', password);
                this.setState({isLogged: true});
            }
        })
        .catch(e => console.log(e));
    }

    handleClickLogout = () => {
        localStorage.removeItem('username');
        localStorage.removeItem('password');
        this.setState({isLogged: false});
    }

    renderNotLoggedView() {
        return (
            <div className="sidenav">
                <div className="login-main-text">
                            <h2><br /> Kirjautuminen hallintapaneeliin</h2>
                        </div>
                    <div className="main">
                    <div className="col-md-6 col-sm-12">
                        <div className="login-form">
                        <form>
                            <div className="form-group">
                                <label>User Name</label>
                                <input 
                                name="user"
                                value={this.state.user}
                                onChange={this.changeText}
                                type="text" 
                                className="form-control" 
                                placeholder="User Name"></input>
                            </div>
                            <div className="form-group">
                                <label>Password</label>
                                <input
                                name="password"
                                value={this.state.password}
                                onChange={this.changeText}
                                type="password" 
                                className="form-control" 
                                placeholder="Password"
                                ></input>
                                
                            </div>
                            <button onClick={() => this.handleClickLogin(this.state.user, this.state.password)} type="button" className="btn btn-black">Login</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            
        )
    }
    
  

    renderLoggedView() {
        return (
            <div>
                <ControlPanel/>
                <Button onClick={this.handleClickLogout} className="btn btn-black">Log Out</Button>
            </div>
        )
    }

    render() {
        return (
            <>
            <Header />
            <div className="container">
                <div className= "login">
                    {this.isLogged() ? this.renderLoggedView() : this.renderNotLoggedView()}
                </div>
            </div>
            <Footer/>
            </>
        )
    }
}
