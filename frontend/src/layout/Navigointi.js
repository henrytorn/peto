import React, {Component } from 'react';


export default class Navigointi extends Component{

    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="navbar-brand" href="/"><img id="logo" src="PeTo-logo.png" alt="logo"/></div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item active" id="navigointiLinkit">
                        <a className="nav-link" href="/">Kotisivu <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item active" id="navigointiLinkit">
                        <a className="nav-link" href="/Ajankohtaista">Ajankohtaista <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item active" id="navigointiLinkit">
                        <a className="nav-link" href="/Joukkue">Joukkue <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item active" id="navigointiLinkit">
                        <a className="nav-link" href="/Otteluohjelma">Otteluohjelma <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item active" id="navigointiLinkit">
                        <a className="nav-link" href="/Yhteystiedot">Yhteystiedot <span className="sr-only">(current)</span></a>
                    </li>
                    </ul>
                </div>
            </nav>

        )
    }
}