import React from "react";
import { Table, Tab, Tabs, Button, Jumbotron, Form, ListGroup } from 'react-bootstrap';
import axios from 'axios';
import { BACKEND_URL } from '../constants';
import RichTextEditor from "../components/RichTextEditor";
import {FormItem} from '../components/FormItem'

export default class ControlPanel extends React.Component{
    componentDidMount() {
        axios.get(`${BACKEND_URL}/players`)
        .then(res => this.setState({players: res.data}))
        .catch(e => console.log(e))

        axios.get(`${BACKEND_URL}/sponsors`)
        .then(res => this.setState({sponsors: res.data}))
        .catch(e => console.log(e))
        this.getUsers();

        axios.get(`${BACKEND_URL}/news`)
        .then(res => this.setState({news: res.data}))
        .catch(e => console.log(e))
        this.getUsers();
    }
    state = {
        players: [],
        sponsors: [],
        news:[],
        imgUrl: "",
        url: "",
        id: "",
        firstName: "",
        lastName: "",
        number: "",
        position: "",
        id: "",
        linkUrl: "", 
        selectedPlayer: {},
        selectedSponsor: "",
        newsTitle: "",
        newsContent: ""
    }

    changeText = e => this.setState({ [e.target.name]: e.target.value });

    changeSelectedFirstName = e => {
      this.state.selectedPlayer.firstName = e.target.value;
      this.setState({
        selectedPlayer: this.state.selectedPlayer
      });
    };

    changeSelectedLastName = e => {
      this.state.selectedPlayer.lastName = e.target.value;
      this.setState({
        selectedPlayer: this.state.selectedPlayer
      });
    };

    changeSelectedNumber = e => {
      this.state.selectedPlayer.number = e.target.value;
      this.setState({
        selectedPlayer: this.state.selectedPlayer
      });
    };

    changeSelectedPosition = e => {
      this.state.selectedPlayer.position = e.target.value;
      this.setState({
        selectedPlayer: this.state.selectedPlayer
      });
    };

    changeSelectedLinkUrl = e => {
      this.state.selectedPlayer.linkUrl = e.target.value;
      this.setState({
        selectedPlayer: this.state.selectedPlayer
      });
    };

    changeSelectedImgUrl = e => {
      this.state.selectedSponsor.imgUrl = e.target.value;
      this.setState({
        selectedSponsor: this.state.selectedSponsor
      });
    };

    changeSelectedUrl = e => {
      this.state.selectedSponsor.url = e.target.value;
      this.setState({
        selectedSponsor: this.state.selectedSponsor
      });
    };

    selectPlayer(player) {
      this.setState({
        selectedPlayer: player
      });
    }
  
    selectSponsor(sponsor) {
      this.setState({
        selectedSponsor: sponsor
      });
    }
  

    getUsers = () => {
        axios.get(`${BACKEND_URL}/players`).then(res =>
          this.setState({
            players: res.data
          })
        );
      };

      getNews = () => {
        axios.get(`${BACKEND_URL}/news`).then(res =>
          this.setState({
            news: res.data
          })
        );
      };

      getSponsors = () => {
        axios.get(`${BACKEND_URL}/sponsors`).then(res =>
          this.setState({
            sponsors: res.data
          })
        );
      };
    

    createSponsor = () => {
      let postSponsor = {
        imgUrl: this.state.imgUrl,
        url: this.state.url
        
      };
      axios
        .post(`${BACKEND_URL}/sponsors`, postSponsor)
        .then(res => this.getSponsors());
        console.log("ok lisäys")
    };

    acceptChange() {
      axios
        .put(
          `${BACKEND_URL}/players/` + this.state.selectedPlayer.id,
          this.state.selectedPlayer
        )
        .then(result => {
          this.getUsers();
          this.setState({
            selectedPlayer: ""
          });
        });
    }

    acceptChangeSponsor() {
      axios
        .put(
          `${BACKEND_URL}/sponsors/` + this.state.selectedSponsor.id,
          this.state.selectedSponsor
        )
        .then(result => {
          this.getSponsors();
          this.setState({
            selectedSponsor: ""
          });
        });
    }
  

    createUser = () => {
        let post = {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          number: this.state.number,
          position: this.state.position,
          linkUrl: this.state.linkUrl
        };
        axios
          .post(`${BACKEND_URL}/players`, post)
          .then(res => {
            alert("Pelaajan lisääminen onnistui.")
            this.getUsers()
          });
      };

handleClick(userId) {
  let muokattava = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      number: this.state.number,
      position: this.state.position,
      linkUrl: this.state.linkUrl
  }
    axios.delete(`${BACKEND_URL}/players/` + userId).then(result => {
      this.getUsers();
      console.log(result);
    });
  }

  handleClickSponsor(sponsorId) {
    let muokattava = {
        imgUrl: this.state.imgUrl,
        url: this.state.url,
    }
      axios.delete(`${BACKEND_URL}/sponsors/` + sponsorId).then(result => {
        this.getSponsors();
        console.log(result);
      });
    }
    
    createNews = () => {
      let uusiUutinen = {
        title: this.state.newsTitle,
        content: this.state.newsContent
        
      };
      axios
      .post(`${BACKEND_URL}/news`, uusiUutinen)
      .then(res => {
        alert("uutisen lisääminen onnistui.")
        this.getNews()
      });
  };

    handleClickNews(newsId) {
      let muokattava = {
        title: this.state.newsTitle,
        content: this.state.newsContent,
    }
        axios.delete(`${BACKEND_URL}/news/` + newsId).then(result => {
          this.getNews();
          console.log(result);
        });
      }

    renderPlayersEdit() {
      return (
        <Table striped bordered hover>
        <thead>
          <tr>
            <th width="15px">Pelinumero</th>
            <th>#</th>
            <th>Etunimi</th>
            <th>Sukunimi</th>
            <th>Pelipaikka</th>
          </tr>
        </thead>
        <tbody>
          {this.state.players.map(player => (
            <tr>
              <td>
              <Button variant="danger" onClick={() => {this.handleClick(player.id);}}>
                  Poista
                </Button>

              </td>
              <td>{player.number}</td>
              <td>{player.firstName}</td>
              <td>{player.lastName}</td>
              <td>{player.position}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      )
    }

    renderSponsorsEdit() {
      return(
        <div>
        <Jumbotron>
          <ListGroup>
            {this.state.selectedSponsor ? (
              <div>
                Valittu
                <Form.Control
                  value={this.state.selectedSponsor.imgUrl}
                  onChange={this.changeSelectedImgUrl}
                  type="text"
                  name="imgUrl"
                />
                <Form.Control
                  value={this.state.selectedSponsor.url}
                  onChange={this.changeSelectedUrl}
                  type="text"
                  name="url"
                />
                <Button
                  onClick={() => {
                    this.acceptChangeSponsor();
                  }}
                >
                  OK
                </Button>
              </div>
            ) : (
              <div></div>
            )}
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>KuvaURL</th>
                  <th>Yritys URL</th>
                  <th>'''</th>
                </tr>
              </thead>
              <tbody>
                {this.state.sponsors.map(sponsor => (
                  <tr>
                    <td style={{ wordBreak:'break-word'}}>{sponsor.imgUrl}</td>
                    <td>{sponsor.url}</td>
                    <td>
                      <Button
                        onClick={() => {
                          this.selectSponsor(sponsor);
                        }}
                      >
                        Muokkaa
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </ListGroup>
        </Jumbotron>
      </div>
      )
    }

    renderAddPlayer() {
      return (
        <div>
              <Jumbotron>
                <Form>
                  <FormItem
                    label='Pelaajan etunimi'
                    value={this.state.firstName}
                    onChange={this.changeText}
                    placeholder="Matti"
                    name="firstName"
                  />
                  <FormItem
                    label='Pelaajan sukunimi'
                    value={this.state.lastName}
                    onChange={this.changeText}
                    placeholder="Meikäläinen"
                    name="lastName"
                  />
                  <FormItem
                    label='Pelaajan pelinumero'
                    value={this.state.number}
                    onChange={this.changeText}
                    placeholder="#99"
                    name="number"
                  />
                  <FormItem
                    label='Pelaajan pelipaikka'
                    value={this.state.position}
                    onChange={this.changeText}
                    placeholder="Maalivahti / Puolustaja / Hyökkääjä"
                    name="position"
                  />
                
                <FormItem
                    label='Pelaajan Eliteprospects URL'
                    value={this.state.linkUrl}
                    onChange={this.changeText}
                    placeholder="https://www.eliteprospects.com/player/pelaajaID/aku-ankka"
                    name="linkUrl"
                  />

                  <Button onClick={this.createUser} variant="success">
                    Lähetä
                  </Button>
                </Form>
              </Jumbotron>
            </div>
      )
    }

    renderSponsors() {
      return(
        <div>
        <Jumbotron>
          <ListGroup>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>kuvaurl</th>
                  <th>url</th>
                </tr>
              </thead>
              <tbody>
                {this.state.sponsors.map(sponsor => (
                  <tr>
                    <td>
                    <Button variant="danger" onClick={() => {this.handleClickSponsor(sponsor.id);}}>
                        Poista
                      </Button>
                    </td>
                    <td style={{ wordBreak:'break-word'}}>{sponsor.imgUrl}</td>
                    <td>{sponsor.url}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </ListGroup>
        </Jumbotron>
      </div>
      )
    }

    renderNewsEdit() {
      return (
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>title</th>
            <th>Content</th>
          </tr>
        </thead>
        <tbody>
          {this.state.news.map(uutinen => (
            <tr>
              <td>
              <Button variant="danger" onClick={() => {this.handleClickNews(uutinen.id);}}>
                        Poista
                      </Button>
              </td>
              <td>{uutinen.title}</td>
              <td>{uutinen.content}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      )
    }

    render(){
        return(

          /****************PELAAJIEN MUOKKAUS JA POISTO*****************/
    <div className="Content">
    <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
        <Tab eventKey="home" title="Pelaajat">
          {this.renderPlayersEdit()}
        </Tab>

      {/****************** PELAAJIEN LISÄYS *******************/}
        <Tab eventKey="Lisää Pelaaja" title="Lisää Pelaaja">
          {this.renderAddPlayer()}
        </Tab>

        <Tab eventKey="muokkaa" title="Muokkaa pelaaja">
            <div>
              <Jumbotron>
                <ListGroup>
                  {this.state.selectedPlayer ? (
                    <div>
                      Valittu
                      <Form.Control
                        value={this.state.selectedPlayer.firstName}
                        onChange={this.changeSelectedFirstName}
                        type="text"
                        name="name"
                      />
                      <Form.Control
                        value={this.state.selectedPlayer.lastName}
                        onChange={this.changeSelectedLastName}
                        type="text"
                        name="name"
                      />
                      <Form.Control
                        value={this.state.selectedPlayer.number}
                        onChange={this.changeSelectedNumber}
                        type="text"
                        name="name"
                      />
                      <Form.Control
                        value={this.state.selectedPlayer.position}
                        onChange={this.changeSelectedPosition}
                        type="text"
                        name="name"
                      />
                       <Form.Control
                        value={this.state.selectedPlayer.linkUrl}
                        onChange={this.changeSelectedLinkUrl}
                        type="text"
                        name="name"
                      />
                      <Button
                        onClick={() => {
                          this.acceptChange();
                        }}
                      >
                        OK
                      </Button>
                    </div>
                  ) : (
                    <div></div>
                  )}
                  <Table striped bordered hover>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Etunimi</th>
                        <th>Sukunimi</th>
                        <th>Pelipaikka</th>
                        <th>EliteURL</th>
                        <th>'''</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.players.map(player => (
                        <tr>
                          <td>{player.number}</td>
                          <td>{player.firstName}</td>
                          <td>{player.lastName}</td>
                          <td>{player.position}</td>
                          <td>{player.linkUrl}</td>
                          <td>
                            <Button
                              onClick={() => {
                                this.selectPlayer(player);
                              }}
                            >
                              Muokkaa
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </ListGroup>
              </Jumbotron>
            </div>
          </Tab>


        <Tab eventKey="Uusi uutinen" title="Uusi uutinen">
               <RichTextEditor text={this.state.newsTitle} onChange={(text) => this.setState({newsTitle: text})}/> 
               <RichTextEditor text={this.state.newsContent} onChange={(text) => this.setState({newsContent: text})}/> 
               
               <Button onClick={this.createNews} variant="success">
                    Lähetä
                  </Button>
                  {this.renderNewsEdit()}
        </Tab>

        <Tab eventKey="Sponsorit" title="Sponsorit">
        <Jumbotron>
                <Form>

                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Yrityksen logoURL</Form.Label>
                    <Form.Control
                      value={this.state.imgUrl}
                      onChange={this.changeText}
                      type="text"
                      placeholder="www.kuva.com/kuva22.png"
                      name="imgUrl"
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>

                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Yrityksen kotisivuURL</Form.Label>
                    <Form.Control
                      value={this.state.url}
                      onChange={this.changeText}
                      type="text"
                      placeholder="www.yritys.fi"
                      name="url"
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>
                  
                  <Button onClick={this.createSponsor} variant="success">
                    Lähetä
                  </Button>
                </Form>
              
              {this.renderSponsors()}
              </Jumbotron>
          </Tab>

        <Tab eventKey="muokkaa sponsoria" title="Muokkaa sponsori">
        {this.renderSponsorsEdit()}
          </Tab>

    </Tabs>
            </div>
        )
    }
}