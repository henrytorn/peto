import React from "react";
import { BACKEND_URL } from '../constants';
import axios from 'axios'


export default class Footer extends React.Component{
    componentDidMount() {
        // Sivu ladataan ensimmäistä kertaa
        // Joukkueen sivun tiedot haetaan b"ackendiltä ja asetetaan luokan tilaan
        axios.get(`${BACKEND_URL}/sponsors`)
        .then(res => this.setState({sponsors: res.data}))
        .catch(e => console.log(e))
    }
    state = {
        sponsors: []
    }
    
    renderSponsor(sponsor){
        return(

            <a key={sponsor.id} target="_blank" rel="noopener noreferrer" href={sponsor.url}><img className="spnrs" alt="kuva" src={`${sponsor.imgUrl}`}></img></a>

        )
    }
    render() {
           
        return ( 
         <>
            <div className="footer"> 
                <div className="navbar-bottom text-white bg-dark" >
                    {this.state.sponsors.map(sponsor=> this.renderSponsor(sponsor))}
                </div>
            </div>
         </>
         )
 }
}