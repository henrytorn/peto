import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom' 


// Sivut
import  KotiSivu from './pages/KotiSivu';
import Joukkue  from './pages/Joukkue';
import Ajankohtaista  from './pages/Ajankohtaista';
import  OtteluOhjelma  from './pages/OtteluOhjelma';
import YhteysTiedot  from './pages/YhteysTiedot';
import Admin from './pages/Admin';

export default class App extends Component {
  
  render(){

    return (
      <div className="App">
        <React.Fragment>
          <Router>
            <Switch>
              <Route exact path="/" component={KotiSivu}/>
              <Route path="/Ajankohtaista" component={Ajankohtaista}/>
              <Route path="/Joukkue" component={Joukkue}/>
              <Route path="/Otteluohjelma" component={OtteluOhjelma}/>
              <Route path="/Yhteystiedot" component={YhteysTiedot}/>
              <Route path="/admin" component={Admin}/>
            </Switch>
          </Router>
        </React.Fragment>
        </div>
    );
  }
}