import React from 'react';
import { Form} from 'react-bootstrap';

export const FormItem = (props) => {
    return (
        <Form.Group controlId="formBasicEmail">
        <Form.Label>{props.label}</Form.Label>
        <Form.Control
          value={props.value}
          onChange={props.onChange}
          type="text"
          placeholder={props.placeholder}
          name={props.name}
        />
        <Form.Text className="text-muted"></Form.Text>
      </Form.Group>
    )
}